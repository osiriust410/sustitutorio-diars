﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Susti709912.Domain;

namespace Susti709912.Repository
{
    public class Susti709912Repository:ISusti709912Repository
    {
        private Susti709912Context _context = new Susti709912Context();
        public List<Proyecto> Proyectos(DateTime? fechaFin, DateTime? fechaInicio)
        {
            var query = from c in _context.Proyecto.Include(x => x.Supervisor).Include(x => x.Apr)
                        select c;
            if (fechaFin.HasValue && fechaInicio.HasValue)
            {
                query = from c in query
                        where
                        (c.FechaInicio >= fechaInicio.Value && c.FechaInicio <= fechaFin.Value) ||
                        (c.FechaFin >= fechaInicio.Value && c.FechaFin <= fechaFin.Value)
                        select c;
            }
            return query.ToList();
        }


        public void CrearProyecto(Proyecto proyecto)
        {
            _context.Proyecto.Add(proyecto);
            _context.SaveChanges();
        }


        public List<Trabajador> Trabajador()
        {
            var trabajador = _context.Trabajador;
            return trabajador.ToList();
        }
    }
}
