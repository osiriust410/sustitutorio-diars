﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susti709912.Domain;

namespace Susti709912.Repository
{
    public interface ISusti709912Repository
    {
        List<Proyecto> Proyectos(DateTime? fechaFin, DateTime? fechaInicio);
        void CrearProyecto(Proyecto proyecto);
        List<Trabajador> Trabajador();
    }
}
