namespace Susti709912.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Proyecto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 200),
                        SupervisorId = c.Int(nullable: false),
                        AprId = c.Int(nullable: false),
                        FechaInicio = c.DateTime(nullable: false),
                        FechaFin = c.DateTime(nullable: false),
                        IsActivo = c.Boolean(nullable: false),
                        Comentario = c.String(nullable: false, unicode: false, storeType: "text"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trabajador", t => t.AprId)
                .ForeignKey("dbo.Trabajador", t => t.SupervisorId)
                .Index(t => t.SupervisorId)
                .Index(t => t.AprId);
            
            CreateTable(
                "dbo.Trabajador",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Dni = c.String(nullable: false, maxLength: 8),
                        Nombres = c.String(nullable: false, maxLength: 200),
                        Apellidos = c.String(nullable: false, maxLength: 200),
                        Direccion = c.String(nullable: false, maxLength: 200),
                        Sexo = c.String(nullable: false, maxLength: 1, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proyecto", "SupervisorId", "dbo.Trabajador");
            DropForeignKey("dbo.Proyecto", "AprId", "dbo.Trabajador");
            DropIndex("dbo.Proyecto", new[] { "AprId" });
            DropIndex("dbo.Proyecto", new[] { "SupervisorId" });
            DropTable("dbo.Trabajador");
            DropTable("dbo.Proyecto");
        }
    }
}
