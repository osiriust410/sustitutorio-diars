// <auto-generated />
namespace Susti709912.Repository.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateDB : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateDB));
        
        string IMigrationMetadata.Id
        {
            get { return "201612170107039_CreateDB"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
