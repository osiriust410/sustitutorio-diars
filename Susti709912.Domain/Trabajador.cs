﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Susti709912.Domain
{
    public class Trabajador
    {
        public int Id { get; set; }
        [Required]
        [StringLength(8)]
        public string Dni { get; set; }
        [Required]
        [StringLength(200)]
        public string Nombres { get; set; }
        [Required]
        [StringLength(200)]
        public string Apellidos { get; set; }
        [Required]
        [StringLength(200)]
        public string Direccion { get; set; }
        [Required]
        [StringLength(1)]
        [Column(TypeName = "nchar")]
        public string Sexo { get; set; }
        public string NombresCompletos
        {
            get { return Nombres + " " + Apellidos; }
        }
    }
}
