﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Susti709912.Domain
{
    public class Proyecto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Nombre { get; set; }
        public int SupervisorId { get; set; }
        public Trabajador Supervisor { get; set; }
        public int AprId { get; set; }
        public Trabajador Apr { get; set; }
        [Required]
        [Display(Name = "Fecha Fin")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaInicio { get; set; }
        [Required]
        [Display(Name = "Fecha Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime FechaFin { get; set; }
        [Required]
        [Display(Name = "Activo")]
        public bool IsActivo { get; set; }
        [Required]
        [Column(TypeName = "text")]
        public string Comentario { get; set; }
    }
}
