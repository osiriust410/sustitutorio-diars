﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Susti709912.Domain;
using Susti709912.Service;

namespace Susti709912.Web.Controllers
{
    public class HomeController : Controller
    {
        private ISusti709912Service _service;

        public HomeController(ISusti709912Service service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult Index(DateTime? fechaFin, DateTime? fechaInicio, int? page)
        {
            ViewBag.Page = page;
            var pageIndex = page ?? 1;
            var lista = _service.Proyectos(fechaFin, fechaInicio).ToPagedList(pageIndex, 3);
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }
        public ActionResult CrearProyecto()
        {
            var trabajadores = _service.Trabajador();
            ViewBag.Trabajadores = new SelectList(trabajadores, "Id", "NombresCompletos");
            return View(new Proyecto());
        }
        [HttpPost]
        public ActionResult CrearProyecto(Proyecto proyecto)
        {
            var trabajadores = _service.Trabajador();
            ViewBag.Trabajadores = new SelectList(trabajadores, "Id", "NombresCompletos");
            if (ModelState.IsValid)
            {
                _service.CrearProyecto(proyecto);
                return RedirectToAction("Index");
            }
            return View(proyecto);
        }

       
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}