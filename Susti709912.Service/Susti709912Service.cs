﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Susti709912.Domain;
using Susti709912.Repository;

namespace Susti709912.Service
{
    public class Susti709912Service:ISusti709912Service
    {
        private ISusti709912Repository _repository;

        public Susti709912Service(ISusti709912Repository repository)
        {
            _repository = repository;
        }
        public List<Proyecto> Proyectos(DateTime? fechaFin, DateTime? fechaInicio)
        {
            return _repository.Proyectos(fechaFin, fechaInicio);
        }


        public void CrearProyecto(Proyecto proyecto)
        {
            _repository.CrearProyecto(proyecto);
        }


        public List<Trabajador> Trabajador()
        {
            return _repository.Trabajador();
        }
       
    }
}
